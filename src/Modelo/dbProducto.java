package Modelo;

import Vista.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class dbProducto extends dbManejador implements dbPersistencia {

    VistaMostrar vista;


    @Override
    public void insertar(Object objeto) throws Exception {
        ventas ven = (ventas) objeto;

        String consulta = "INSERT INTO sistema.ventas(codigo, tipo, precio,cantidad,total) VALUES (?, ?, ?, ?, ?)";

        if (this.conectar()) {
            try {
                PreparedStatement statement = conexion.prepareStatement(consulta);
                statement.setString(1, ven.getCodigo());
                statement.setInt(2, ven.getTipo());
                statement.setDouble(3, ven.getPrecio());
                statement.setInt(4, ven.getCantidad());
                statement.setDouble(5, ven.getTotal());
                statement.executeUpdate();
                this.desconectar();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(vista, "Surgió un error al insertar: " + e.getMessage());
            }
        } else {
            JOptionPane.showMessageDialog(vista, "No fue posible conectarse");
        }
    }
    
    @Override
    public void actualizar(Object objecto) throws Exception {
        ventas ven = new ventas();
        ven = (ventas) objecto;
        String consulta = "UPDATE sistema.ventas SET tipo = ?, precio = ?, cantidad = ?, total = ? WHERE codigo = ?";
        if (this.conectar()) {
            try {
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                this.sqlConsulta.setInt(1, ven.getTipo());
                this.sqlConsulta.setDouble(2, ven.getPrecio());
                this.sqlConsulta.setInt(3, ven.getCantidad());
                this.sqlConsulta.setDouble(4, ven.getTotal());
                this.sqlConsulta.setString(5, ven.getCodigo());
                this.sqlConsulta.executeUpdate();
                this.desconectar();
                JOptionPane.showMessageDialog(vista,"Se actualizó correctamente");

            }catch (SQLException e) {
                JOptionPane.showMessageDialog(vista,"Surgio un error al actualizar" +e.getMessage());
            }
        }
    }

    @Override
    public DefaultTableModel listar() throws Exception {
        if (this.conectar()) {
            try {
                String sql = "select * from sistema.ventas order by id";
                DefaultTableModel Tabla = new DefaultTableModel();
                PreparedStatement pst = conexion.prepareStatement(sql);
                ResultSet rs = pst.executeQuery();
                ResultSetMetaData Datos = rs.getMetaData();

                //Agregar Columnas
                for (int column = 1; column <= Datos.getColumnCount(); column++) {
                    Tabla.addColumn(Datos.getColumnLabel(column));
                }
                //Agregar Tablas
                while (rs.next()) {
                    Object[] row = new Object[Datos.getColumnCount()];
                    for (int column = 1; column <= Datos.getColumnCount(); column++) {
                        row[column - 1] = rs.getString(column);
                    }
                    Tabla.addRow(row);
                }
                return Tabla;
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(vista, "Surgio un error al listar" + e.getMessage());
            }
        }
        return null;
    }

    @Override
    public Object buscar(String codigo) throws Exception {
        ventas ven = new ventas();
        if (this.conectar()) {
            try {
                String consultar = "Select * from sistema.ventas where codigo= ?";
                this.sqlConsulta = this.conexion.prepareStatement(consultar);
                this.sqlConsulta.setString(1, codigo);
                this.Registros = this.sqlConsulta.executeQuery();
                if (this.Registros.next()) {
                    ven.setCodigo(this.Registros.getString("codigo"));
                    ven.setTipo(this.Registros.getInt("tipo"));
                    ven.setPrecio(this.Registros.getDouble("precio"));
                    ven.setCantidad(this.Registros.getInt("cantidad"));
                    ven.setTotal(this.Registros.getDouble("total"));
                }
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(vista, "Surgio un error al buscar" + e.getMessage());
            }

        }
        this.desconectar();
        return ven;
    }

    

}