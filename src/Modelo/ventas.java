
package Modelo;

public class ventas {
    private int id;
    private String codigo;
    private double precio; 
    private int cantidad;
    private int tipo;
    private double total;

    public ventas(int id, String codigo, double precio, int cantidad, int tipo, double total) {
        this.id = id;
        this.codigo = codigo;
        this.precio = precio;
        this.cantidad = cantidad;
        this.tipo = tipo;
        this.total = total;
    }

    public ventas(ventas ven) {
        this.id = ven.id;
        this.codigo = ven.codigo;
        this.precio = ven.precio;
        this.cantidad = ven.cantidad;
        this.tipo = ven.tipo;
        this.total = ven.total;
    }

    public ventas() {
        this.id = 0;
        this.codigo = "";
        this.precio = 0.0d;
        this.cantidad = 0;
        this.tipo = 0;
        this.total = 0.0d;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public void setTotal(double total) {
        this.total = calcularTotal();
    }

    public int getId() {
        return id;
    }

    public String getCodigo() {
        return codigo;
    }

    public double getPrecio() {
        return precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public int getTipo() {
        return tipo;
    }

    public double getTotal() {
        return total;
    }
    
    public double calcularTotal(){
        if(getTipo() ==  0){
            setPrecio(24.5);
        }
        else if(getTipo() == 1){
            setPrecio(20.5);
        }
        return getCantidad()*getPrecio();
    }

}
