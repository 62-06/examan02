package Controlador;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import Modelo.ventas;
import Modelo.dbProducto;
import Vista.VistaMostrar;
import Vista.VistaVentas;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Controlador implements ActionListener {

    private VistaMostrar vistam;
    private VistaVentas vistav;
    private dbProducto dbpro;
    private ventas vent;

    private boolean isInsertar = false;

    public Controlador(VistaMostrar vistam, VistaVentas vistav, dbProducto dbpro, ventas vent) {
        this.vistam = vistam;
        this.vistav = vistav;
        this.dbpro = dbpro;
        this.vent = vent;

        vistav.btnBuscar.addActionListener(this);
        vistav.btnMostrar.addActionListener(this);
        vistav.btnGuardar.addActionListener(this);
        vistav.btnNuevo.addActionListener(this);
        vistav.btnRealizar.addActionListener(this);
        vistav.cbmTipos.addActionListener(this);
        vistam.btnCerrar.addActionListener(this);
    }

    private void iniciarVista() {
        try {
            deshabilitar();

            vistav.setTitle("Datos de gasolina");
            vistav.setSize(600, 600);
            vistav.setVisible(true);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(vistav, "Error al iniciar la vista: " + ex.getMessage());
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vistav.cbmTipos) {
            switch (vistav.cbmTipos.getSelectedIndex()) {
                case 0:
                    vistav.lblPrecio.setText("24.50");
                    break;
                case 1:
                    vistav.lblPrecio.setText("20.50");
                    break;
            }
        } else if (e.getSource() == vistav.btnBuscar) {
            if (vistav.txtCodigo.getText().isEmpty()) {
                JOptionPane.showMessageDialog(vistav, "Ingrese un codigo para buscars");
            } else {
                try {
                    vent = (ventas) dbpro.buscar(vistav.txtCodigo.getText());
                    System.out.println(vent.getCantidad());
                    vistav.cbmTipos.setSelectedIndex(vent.getTipo());
                    vistav.txtCantidad.setText(String.valueOf(vent.getCantidad()));
                    vistav.lblCantidad.setText(String.valueOf(vent.getCantidad()));
                    vistav.lblTotal.setText(String.valueOf(vent.getTotal()));
                    isInsertar = true;
                } catch (Exception ex) {
                    Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        } else if (e.getSource() == vistav.btnNuevo) {
            habilitar();
        } else if (e.getSource() == vistav.btnMostrar) {
            try {
                vistam.tblVentasRealizadas.setModel(dbpro.listar());
            } catch (Exception ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
            vistav.setVisible(false);
            vistam.setVisible(true);
        } else if (e.getSource() == vistam.btnCerrar) {
            if (JOptionPane.showConfirmDialog(vistam, "¿Desea cerrar?", "Cerrar", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                vistam.setVisible(false);
                vistam.dispose();
                System.exit(0);
            }
        } else if (e.getSource() == vistav.btnGuardar) {
            if (!isEmpty()) {
                JOptionPane.showMessageDialog(vistav, "No deje ningun espacio en vacio");
            } else {
                if (!vent.getCodigo().equals("")) {
                    JOptionPane.showMessageDialog(vistav, "Ya existe");
                } else {
                    if (!isInsertar) {
                        try {
                            vent.setCodigo(vistav.txtCodigo.getText());
                            vent.setTipo(vistav.cbmTipos.getSelectedIndex());
                            if (isNumeric(vistav.txtCantidad.getText())) {
                                vent.setCantidad(Integer.parseInt(vistav.txtCantidad.getText()));
                                vent.setTotal(vent.calcularTotal());
                                System.out.println(vent.getCantidad() + " " + vent.getTipo() + " " + vent.getPrecio() + " " + vent.getTotal());
                                dbpro.insertar(vent);
                                JOptionPane.showMessageDialog(vistav, "Se guardó correctamente.");
                            } else {
                                JOptionPane.showMessageDialog(vistav, "La cantidad debe ser un valor numérico.");
                            }
                        } catch (Exception ex) {
                            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        try {
                            vent.setCodigo(vistav.txtCodigo.getText());
                            vent.setTipo(vistav.cbmTipos.getSelectedIndex());
                            if (isNumeric(vistav.txtCantidad.getText())) {
                                vent.setCantidad(Integer.parseInt(vistav.txtCantidad.getText()));
                                vent.setTotal(vent.calcularTotal());
                                System.out.println(vent.getCantidad() + " " + vent.getTipo() + " " + vent.getPrecio() + " " + vent.getTotal());
                                dbpro.insertar(vent);
                                JOptionPane.showMessageDialog(vistav, "Se actualizo correctamente.");
                            } else {
                                JOptionPane.showMessageDialog(vistav, "La cantidad debe ser un valor numérico.");
                            }
                        } catch (Exception ex) {
                            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }

        }
        if (e.getSource() == vistav.btnRealizar) {
            vent.setCantidad(Integer.parseInt(vistav.txtCantidad.getText()));
            vent.setPrecio(Float.parseFloat(vistav.lblPrecio.getText()));
            vistav.lblCantidad.setText(String.valueOf(vent.getCantidad()));
            vistav.lblPrecio.setText(String.valueOf(vent.getPrecio()));
            vistav.lblTotal.setText(String.valueOf(vent.calcularTotal()));

        }
    }
    

    

    public boolean isEmpty() {
        return !vistav.txtCodigo.getText().isEmpty() && !vistav.txtCantidad.getText().isEmpty();
    }

    public void limpiar() {
        vistav.txtCantidad.setText("");
        vistav.txtCodigo.setText("");
    }

    public void deshabilitar() {
        vistav.btnBuscar.setEnabled(false);
        vistav.btnGuardar.setEnabled(false);
        vistav.btnMostrar.setEnabled(false);
        vistav.btnRealizar.setEnabled(false);
        vistav.txtCodigo.setEnabled(false);
        vistav.cbmTipos.setEnabled(false);
    }

    public void habilitar() {
        vistav.btnBuscar.setEnabled(true);
        vistav.btnGuardar.setEnabled(true);
        vistav.btnMostrar.setEnabled(true);
        vistav.btnRealizar.setEnabled(true);
        vistav.txtCantidad.setEnabled(true);
        vistav.txtCodigo.setEnabled(true);
        vistav.cbmTipos.setEnabled(true);
    }

    private boolean isNumeric(String str) {
        if (str == null) {
            return false;
        }
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static void main(String[] args) {
        dbProducto pro = new dbProducto();
        ventas vent = new ventas();
        VistaMostrar vistamo = new VistaMostrar(new JFrame(), false);
        VistaVentas vistaven = new VistaVentas(new JFrame(), false);
        Controlador con = new Controlador(vistamo, vistaven, pro, vent);
        con.iniciarVista();
    }
}
